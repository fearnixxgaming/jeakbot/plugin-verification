package de.fearnixx.jeak.verification.grants;

import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;

public interface IGrantEntry {

    IQueryRequest grantTo(IClient client);
}
