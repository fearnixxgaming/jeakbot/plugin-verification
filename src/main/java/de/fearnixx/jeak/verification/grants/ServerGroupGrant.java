package de.fearnixx.jeak.verification.grants;

import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;

public class ServerGroupGrant implements IGrantEntry {

    private final int serverGroupId;

    public ServerGroupGrant(int serverGroupId) {
        this.serverGroupId = serverGroupId;
    }

    @Override
    public IQueryRequest grantTo(IClient client) {
        return client.addServerGroup(serverGroupId);
    }
}
