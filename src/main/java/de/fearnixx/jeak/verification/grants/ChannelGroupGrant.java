package de.fearnixx.jeak.verification.grants;

import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.teamspeak.query.IQueryRequest;

public class ChannelGroupGrant implements IGrantEntry {

    private final int channelGroupId;
    private final int channelId;

    public ChannelGroupGrant(int channelGroupId, int channelId) {
        this.channelGroupId = channelGroupId;
        this.channelId = channelId;
    }

    @Override
    public IQueryRequest grantTo(IClient client) {
        return client.setChannelGroup(channelId, channelGroupId);
    }
}
