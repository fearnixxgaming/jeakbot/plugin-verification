package de.fearnixx.jeak.verification;

import de.fearnixx.jeak.verification.VerificationOutcome;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "verifications")
public class VerificationRequest {

    @Id
    @Column
    private String clientUniqueId;

    @Id
    @Column
    private String strategyName;

    @Column
    private LocalDateTime requestedAt;

    @Column
    private LocalDateTime expiresAt;

    @Column
    private String token;

    @Column
    @Enumerated(EnumType.STRING)
    private VerificationOutcome outcome;

    public String getClientUniqueId() {
        return clientUniqueId;
    }

    public void setClientUniqueId(String clientUniqueId) {
        this.clientUniqueId = clientUniqueId;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public LocalDateTime getRequestedAt() {
        return requestedAt;
    }

    public void setRequestedAt(LocalDateTime requestedAt) {
        this.requestedAt = requestedAt;
    }

    public LocalDateTime getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(LocalDateTime expiresAt) {
        this.expiresAt = expiresAt;
    }

    public VerificationOutcome getOutcome() {
        return outcome;
    }

    public void setOutcome(VerificationOutcome outcome) {
        this.outcome = outcome;
    }
}
