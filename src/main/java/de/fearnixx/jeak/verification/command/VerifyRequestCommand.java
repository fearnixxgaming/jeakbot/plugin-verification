package de.fearnixx.jeak.verification.command;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.verification.service.IVerificationService;

public class VerifyRequestCommand implements ICommandReceiver {

    @Inject
    private IVerificationService verificationService;

    @Override
    public void receive(ICommandContext ctx) throws CommandException {

    }
}
