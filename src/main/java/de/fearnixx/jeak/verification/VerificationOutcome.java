package de.fearnixx.jeak.verification;

public enum VerificationOutcome {

    SUCCESS,
    EXPIRED,
    FAILED,
}
