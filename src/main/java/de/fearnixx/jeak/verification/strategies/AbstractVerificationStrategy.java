package de.fearnixx.jeak.verification.strategies;

import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.verification.except.StrategyValidationException;
import de.mlessmann.confort.api.IConfigNode;

public abstract class AbstractVerificationStrategy {

    protected final IConfigNode config;



    public AbstractVerificationStrategy(IConfigNode config) throws StrategyValidationException {
        this.config = config;
        load();
    }

    protected abstract void load() throws StrategyValidationException;

    public abstract boolean verifyToken(IClient client, String token);
}
