package de.fearnixx.jeak.verification.strategies;

import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.verification.except.StrategyValidationException;
import de.mlessmann.confort.api.IConfigNode;

public class SimpleTokenStrategy extends AbstractVerificationStrategy {

    private String token;

    public SimpleTokenStrategy(IConfigNode config) throws StrategyValidationException {
        super(config);
    }

    @Override
    protected void load() throws StrategyValidationException {

    }

    @Override
    public boolean verifyToken(IClient client, String token) {
        return false;
    }
}
