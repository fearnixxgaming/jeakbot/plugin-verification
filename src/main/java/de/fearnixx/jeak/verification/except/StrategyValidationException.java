package de.fearnixx.jeak.verification.except;

public class StrategyValidationException extends Exception {

    public StrategyValidationException() {
        super();
    }

    public StrategyValidationException(String message) {
        super(message);
    }

    public StrategyValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public StrategyValidationException(Throwable cause) {
        super(cause);
    }

    protected StrategyValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
