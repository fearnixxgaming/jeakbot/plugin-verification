package de.fearnixx.jeak.verification.service;

import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.Config;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.teamspeak.data.IClient;
import de.fearnixx.jeak.util.Configurable;
import de.fearnixx.jeak.verification.event.VerificationSuccessEvent;
import de.fearnixx.jeak.verification.strategies.AbstractVerificationStrategy;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VerificationService extends Configurable {

    private static final String PERSISTENCE_UNIT_ID = "verification";
    private static final String DEFAULT_CONF_ID = "/verification/defaultVerifications.json";

    private static final Logger logger = LoggerFactory.getLogger(VerificationService.class);
    
    @Inject
    @Config(id = "verifications")
    private IConfig configRef;

    @Inject
    @PersistenceUnit(name = PERSISTENCE_UNIT_ID)
    private boolean useDB;

    @Inject
    @PersistenceUnit(name = PERSISTENCE_UNIT_ID)
    private EntityManager entityManager;

    @Inject
    private IEventService eventService;

    private String defaultStrategy = "default";
    private Map<String, AbstractVerificationStrategy> strategies = new HashMap<>();

    public VerificationService() {
        super(VerificationService.class);
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONF_ID;
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        return false;
    }

    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {
        if (!loadConfig()) {
            event.cancel();
            return;
        }

        getConfig().getNode("verifications")
                .optMap()
                .orElseGet(() -> {
                    logger.warn("No verifications are registered.");
                    return Collections.emptyMap();
                })
                .forEach(this::loadVerificationStrat);
    }

    private void loadVerificationStrat(String stratName, IConfigNode node) {
        String stratType = node.getNode("type").optString("simple-token");
    }

    public Optional<AbstractVerificationStrategy> getVerificationStrategy(String stratName) {
        return Optional.ofNullable(strategies.getOrDefault(stratName, null));
    }

    public void tryVerification(IClient client, String strategy, String token) {
        tryVerification(client, strategy, token, false);
    }

    public void tryVerification(IClient client, String strategy, String token, boolean forceSuccess) {
        getVerificationStrategy(strategy).ifPresent(strat -> {
            if (forceSuccess || strat.verifyToken(client, token)) {
                eventService.fireEvent(new VerificationSuccessEvent());
            }
        });
    }
}
